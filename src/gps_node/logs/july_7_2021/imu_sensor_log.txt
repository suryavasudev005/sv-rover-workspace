header: 
  seq: 1
  stamp: 
    secs: 1626530909
    nsecs: 681514024
  frame_id: "imu_tf"
orientation: 
  x: -0.31517950899
  y: 0.00715485661202
  z: -0.659868475374
  w: 0.682044192371
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: 0.342957777778
  y: 0.160488888889
  z: 0.0600088888889
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.0101936799185
  y: -0.00101936799185
  z: 0.101936799185
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 2
  stamp: 
    secs: 1626530911
    nsecs: 677464008
  frame_id: "imu_tf"
orientation: 
  x: -0.005049912304
  y: -0.177559840941
  z: -0.322652157744
  w: 0.929700266954
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: -0.272482222222
  y: 0.0136066666667
  z: -0.0383777777778
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.0183486238532
  y: 0.00611620795107
  z: 0.108053007136
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 3
  stamp: 
    secs: 1626530913
    nsecs: 680444955
  frame_id: "imu_tf"
orientation: 
  x: -0.214967309551
  y: 0.614564694979
  z: 0.440304150661
  w: 0.618248773893
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: -0.0551244444444
  y: -0.0997822222222
  z: -0.150371111111
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.013251783894
  y: 0.00203873598369
  z: 0.103975535168
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 4
  stamp: 
    secs: 1626530915
    nsecs: 688277959
  frame_id: "imu_tf"
orientation: 
  x: 0.076838082096
  y: 0.672933762927
  z: 0.0821431049175
  w: -0.731100930219
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: 0.0921066666667
  y: -0.0788488888889
  z: -0.156302222222
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.0193679918451
  y: -0.0
  z: 0.0978593272171
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 5
  stamp: 
    secs: 1626530917
    nsecs: 686202049
  frame_id: "imu_tf"
orientation: 
  x: -0.697660792514
  y: 0.544162213991
  z: 0.331631560585
  w: 0.327379613717
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: 0.267946666667
  y: -0.0509377777778
  z: -0.33284
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.013251783894
  y: -0.00509683995923
  z: 0.100917431193
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 6
  stamp: 
    secs: 1626530919
    nsecs: 696563005
  frame_id: "imu_tf"
orientation: 
  x: 0.28811662237
  y: -0.620987382087
  z: -0.111690671943
  w: -0.720339279092
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: 0.746971111111
  y: -0.366333333333
  z: -0.132926666667
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.0193679918451
  y: -0.00611620795107
  z: 0.0754332313965
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 7
  stamp: 
    secs: 1626530921
    nsecs: 700203895
  frame_id: "imu_tf"
orientation: 
  x: -0.270818917516
  y: -0.598676862982
  z: -0.114464732621
  w: 0.745077816494
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: -0.627651111111
  y: -0.586482222222
  z: 0.00593111111111
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.00917431192661
  y: 0.00407747196738
  z: 0.114169215087
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 8
  stamp: 
    secs: 1626530923
    nsecs: 706156015
  frame_id: "imu_tf"
orientation: 
  x: 0.418689557712
  y: 0.299756904239
  z: 0.529572730842
  w: -0.674090183412
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: -0.0150022222222
  y: 0.145486666667
  z: -0.184911111111
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.00713557594292
  y: -0.00611620795107
  z: 0.098878695209
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 9
  stamp: 
    secs: 1626530925
    nsecs: 710274934
  frame_id: "imu_tf"
orientation: 
  x: -0.119338510339
  y: 0.0187996569
  z: 0.683793081834
  w: 0.719605387755
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: 0.439251111111
  y: 0.580202222222
  z: -1.08225333333
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.0254841997961
  y: -0.0122324159021
  z: 0.110091743119
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 10
  stamp: 
    secs: 1626530927
    nsecs: 713885068
  frame_id: "imu_tf"
orientation: 
  x: -0.691775537628
  y: -0.0497838859743
  z: 0.716683831797
  w: -0.0730236638194
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: 0.00837333333333
  y: 0.687311111111
  z: 0.0516355555556
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.0112130479103
  y: -0.00815494393476
  z: 0.0815494393476
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 11
  stamp: 
    secs: 1626530929
    nsecs: 716538906
  frame_id: "imu_tf"
orientation: 
  x: -0.552784182968
  y: 0.380555719403
  z: 0.61446133861
  w: 0.414782177588
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: -0.595553333333
  y: -0.737202222222
  z: -0.713826666667
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.00305810397554
  y: 0.0
  z: 0.13251783894
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 12
  stamp: 
    secs: 1626530931
    nsecs: 720885038
  frame_id: "imu_tf"
orientation: 
  x: 0.204934643341
  y: -0.160707904227
  z: -0.90176917939
  w: -0.344945080528
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: 0.493677777778
  y: -0.135717777778
  z: -0.151766666667
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.00101936799185
  y: 0.0
  z: 0.117227319062
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 13
  stamp: 
    secs: 1626530933
    nsecs: 724143028
  frame_id: "imu_tf"
orientation: 
  x: 0.016794689316
  y: -0.959778756364
  z: 0.251418207454
  w: -0.123820685687
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: 0.109551111111
  y: -0.0593111111111
  z: -0.0990844444444
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.00407747196738
  y: 0.00101936799185
  z: 0.0886850152905
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 14
  stamp: 
    secs: 1626530935
    nsecs: 727436065
  frame_id: "imu_tf"
orientation: 
  x: 0.529891340161
  y: -0.666553421097
  z: 0.410448485158
  w: 0.326272501873
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: 0.270388888889
  y: 0.0453555555556
  z: 0.305626666667
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.00815494393476
  y: -0.0142711518858
  z: 0.0886850152905
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 15
  stamp: 
    secs: 1626530937
    nsecs: 730412006
  frame_id: "imu_tf"
orientation: 
  x: -0.344939591299
  y: 0.66487594395
  z: 0.38071752067
  w: -0.542227652345
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: 0.145137777778
  y: -0.362495555556
  z: 0.507982222222
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.0152905198777
  y: 0.00203873598369
  z: 0.0733944954128
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 16
  stamp: 
    secs: 1626530939
    nsecs: 731113910
  frame_id: "imu_tf"
orientation: 
  x: -0.195190721446
  y: 0.672206759008
  z: 0.121081771875
  w: 0.703830846103
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: 0.853382222222
  y: -0.0397733333333
  z: 0.903273333333
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: 0.00815494393476
  y: 0.00815494393476
  z: 0.0927624872579
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 17
  stamp: 
    secs: 1626530941
    nsecs: 736655950
  frame_id: "imu_tf"
orientation: 
  x: 0.672787049701
  y: 0.409239292461
  z: -0.615703038717
  w: 0.028116816607
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: -0.0108155555556
  y: 0.328304444444
  z: 0.404362222222
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.0142711518858
  y: -0.00407747196738
  z: 0.110091743119
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 18
  stamp: 
    secs: 1626530943
    nsecs: 736264944
  frame_id: "imu_tf"
orientation: 
  x: 0.531388204321
  y: -0.685239278669
  z: -0.496955144518
  w: 0.0333060297105
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: 1.78142666667
  y: 1.12970222222
  z: 0.594506666667
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.0101936799185
  y: 0.00305810397554
  z: 0.085626911315
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 19
  stamp: 
    secs: 1626530945
    nsecs: 743719100
  frame_id: "imu_tf"
orientation: 
  x: 0.132897067762
  y: -0.436462689695
  z: 0.815151264938
  w: -0.356885282904
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: -0.363193333333
  y: 0.00593111111111
  z: 0.868035555556
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.0112130479103
  y: 0.00917431192661
  z: 0.0968399592253
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 20
  stamp: 
    secs: 1626530947
    nsecs: 746256113
  frame_id: "imu_tf"
orientation: 
  x: -0.0734052555993
  y: 0.872597116401
  z: -0.0758993634878
  w: -0.476891211411
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: -0.845706666667
  y: 0.619626666667
  z: -0.457044444444
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: 0.00203873598369
  y: -0.00713557594292
  z: 0.141692150866
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 21
  stamp: 
    secs: 1626530949
    nsecs: 745011091
  frame_id: "imu_tf"
orientation: 
  x: 0.864460572898
  y: -0.403254192021
  z: -0.11285069513
  w: -0.278134311314
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: -0.504493333333
  y: 0.834193333333
  z: 0.500306666667
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.0122324159021
  y: -0.00509683995923
  z: 0.100917431193
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 22
  stamp: 
    secs: 1626530951
    nsecs: 752104997
  frame_id: "imu_tf"
orientation: 
  x: 0.273858705865
  y: 0.770108826626
  z: 0.247706884282
  w: -0.520168341841
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: 0.97026
  y: -0.21352
  z: 1.76572666667
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: 0.00203873598369
  y: 0.00509683995923
  z: 0.0948012232416
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 23
  stamp: 
    secs: 1626530953
    nsecs: 759495973
  frame_id: "imu_tf"
orientation: 
  x: 0.22357309913
  y: 0.437148981253
  z: 0.583234357597
  w: 0.647111676338
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: 0.129088888889
  y: 0.0310511111111
  z: -0.685566666667
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.013251783894
  y: 0.0142711518858
  z: 0.110091743119
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 24
  stamp: 
    secs: 1626530955
    nsecs: 766567945
  frame_id: "imu_tf"
orientation: 
  x: -0.875921763789
  y: 0.319277185962
  z: -0.215323765484
  w: -0.290617993702
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: -0.294462222222
  y: -0.475186666667
  z: 1.68129555556
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.026503567788
  y: 0.00407747196738
  z: 0.0795107033639
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 25
  stamp: 
    secs: 1626530957
    nsecs: 763127088
  frame_id: "imu_tf"
orientation: 
  x: -0.64983030921
  y: -0.102498455977
  z: 0.749169040821
  w: -0.0772035234241
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: 0.425295555556
  y: 0.173746666667
  z: 0.881642222222
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.00203873598369
  y: 0.0101936799185
  z: 0.0978593272171
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 26
  stamp: 
    secs: 1626530959
    nsecs: 773940086
  frame_id: "imu_tf"
orientation: 
  x: 0.705929576638
  y: -0.107329296756
  z: 0.692353788285
  w: 0.103875342278
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: -0.13816
  y: 1.40078888889
  z: -0.592064444444
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.0101936799185
  y: 0.0203873598369
  z: 0.12130479103
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 27
  stamp: 
    secs: 1626530961
    nsecs: 781048059
  frame_id: "imu_tf"
orientation: 
  x: -0.598476380951
  y: 0.412925411547
  z: -0.37881488415
  w: -0.572553848549
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: 0.128391111111
  y: -0.07536
  z: 0.609508888889
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.0112130479103
  y: 0.00407747196738
  z: 0.123343527013
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 28
  stamp: 
    secs: 1626530963
    nsecs: 780994892
  frame_id: "imu_tf"
orientation: 
  x: -0.711791989547
  y: -0.225152355105
  z: 0.647203962727
  w: -0.154225844908
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: 0.120366666667
  y: -0.04396
  z: -0.71592
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.0254841997961
  y: 0.00611620795107
  z: 0.0998980632008
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 29
  stamp: 
    secs: 1626530965
    nsecs: 786403894
  frame_id: "imu_tf"
orientation: 
  x: -0.635521716864
  y: -0.21528837301
  z: 0.36254271385
  w: 0.646781141094
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: -0.514611111111
  y: 0.316791111111
  z: -0.120715555556
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.0285423037717
  y: -0.00203873598369
  z: 0.0825688073394
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 30
  stamp: 
    secs: 1626530967
    nsecs: 786832094
  frame_id: "imu_tf"
orientation: 
  x: -0.30578484337
  y: -0.806458673524
  z: 0.256064528448
  w: -0.436521471103
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: 0.450764444444
  y: 0.243873333333
  z: 0.471348888889
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.00305810397554
  y: 0.0173292558614
  z: 0.0764525993884
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 31
  stamp: 
    secs: 1626530969
    nsecs: 790249109
  frame_id: "imu_tf"
orientation: 
  x: 0.27604792065
  y: -0.330085644657
  z: 0.900384625666
  w: 0.0644091496619
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: 0.04082
  y: 0.287135555556
  z: 0.707546666667
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.00917431192661
  y: 0.0193679918451
  z: 0.0948012232416
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 32
  stamp: 
    secs: 1626530971
    nsecs: 800668954
  frame_id: "imu_tf"
orientation: 
  x: -0.106756367615
  y: 0.519921027444
  z: 0.691084035457
  w: 0.490599693368
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: -0.91688
  y: -0.639513333333
  z: 2.18927777778
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.0407747196738
  y: 0.0112130479103
  z: 0.150866462793
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 33
  stamp: 
    secs: 1626530973
    nsecs: 802373886
  frame_id: "imu_tf"
orientation: 
  x: 0.773874561554
  y: 0.0185439649848
  z: -0.628734133119
  w: -0.0739437231502
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: -0.00453555555556
  y: 1.15866
  z: -0.832797777778
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.00305810397554
  y: 0.0203873598369
  z: 0.114169215087
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 34
  stamp: 
    secs: 1626530975
    nsecs: 805733919
  frame_id: "imu_tf"
orientation: 
  x: -0.593599651283
  y: -0.180510678579
  z: -0.397656350678
  w: -0.675962111128
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: 1.03864222222
  y: 1.38229777778
  z: 0.646491111111
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.013251783894
  y: 0.00305810397554
  z: 0.124362895005
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 35
  stamp: 
    secs: 1626530977
    nsecs: 809155941
  frame_id: "imu_tf"
orientation: 
  x: -0.631427364755
  y: 0.308414906715
  z: 0.671281641941
  w: 0.235713142501
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: -0.198168888889
  y: -0.619277777778
  z: -0.429133333333
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.0142711518858
  y: 0.00815494393476
  z: 0.103975535168
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 36
  stamp: 
    secs: 1626530979
    nsecs: 822273015
  frame_id: "imu_tf"
orientation: 
  x: 0.659339378066
  y: 0.0344241484115
  z: -0.322387355608
  w: 0.678345749218
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: -0.972702222222
  y: 0.689753333333
  z: -0.536591111111
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.032619775739
  y: 0.00917431192661
  z: 0.118246687054
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 37
  stamp: 
    secs: 1626530981
    nsecs: 824728965
  frame_id: "imu_tf"
orientation: 
  x: -0.517214265147
  y: -0.323736103044
  z: -0.722575242654
  w: 0.324914385982
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: 1.24483555556
  y: 0.0997822222222
  z: -0.326908888889
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.026503567788
  y: -0.00407747196738
  z: 0.0866462793068
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 38
  stamp: 
    secs: 1626530983
    nsecs: 827836990
  frame_id: "imu_tf"
orientation: 
  x: -0.467986394777
  y: 0.651736395487
  z: -0.567103884675
  w: 0.186068775157
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: 0.870128888889
  y: 0.00523333333333
  z: 1.04073555556
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.00407747196738
  y: 0.00203873598369
  z: 0.0754332313965
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 39
  stamp: 
    secs: 1626530985
    nsecs: 829164028
  frame_id: "imu_tf"
orientation: 
  x: -0.240774530184
  y: -0.515900150367
  z: -0.319581793871
  w: 0.757457680331
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: -1.07143777778
  y: 0.959793333333
  z: -2.38465555556
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: 0.00305810397554
  y: -0.0
  z: 0.129459734964
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 40
  stamp: 
    secs: 1626530987
    nsecs: 831196069
  frame_id: "imu_tf"
orientation: 
  x: 0.323312760994
  y: 0.799502366938
  z: 0.490865984352
  w: 0.123755441272
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: -0.56206
  y: -0.291671111111
  z: -0.911995555556
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.00713557594292
  y: 0.0244648318043
  z: 0.119266055046
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 41
  stamp: 
    secs: 1626530989
    nsecs: 837449073
  frame_id: "imu_tf"
orientation: 
  x: -0.561443553112
  y: 0.363630992268
  z: 0.418598595064
  w: 0.614270994221
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: -0.622068888889
  y: 0.278413333333
  z: 0.118622222222
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.0234454638124
  y: -0.00203873598369
  z: 0.0937818552497
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 42
  stamp: 
    secs: 1626530991
    nsecs: 838315963
  frame_id: "imu_tf"
orientation: 
  x: -0.492042743214
  y: 0.154855277317
  z: 0.741455521189
  w: 0.429135750125
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: 0.123506666667
  y: 0.140602222222
  z: 0.484955555556
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.0112130479103
  y: 0.0193679918451
  z: 0.0886850152905
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 43
  stamp: 
    secs: 1626530993
    nsecs: 845571994
  frame_id: "imu_tf"
orientation: 
  x: -0.0377934200476
  y: -0.550414439004
  z: -0.311490911648
  w: -0.773685346053
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: -0.0495422222222
  y: 0.399477777778
  z: 0.387615555556
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.00203873598369
  y: 0.00101936799185
  z: 0.101936799185
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 44
  stamp: 
    secs: 1626530995
    nsecs: 845647096
  frame_id: "imu_tf"
orientation: 
  x: -0.204702981652
  y: 0.756165316237
  z: -0.192455328125
  w: 0.590992090048
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: 0.668471111111
  y: 0.483211111111
  z: -0.680682222222
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.00203873598369
  y: -0.0152905198777
  z: 0.10499490316
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 45
  stamp: 
    secs: 1626530997
    nsecs: 848985910
  frame_id: "imu_tf"
orientation: 
  x: 0.777791051756
  y: -0.323117345511
  z: 0.535231004413
  w: 0.0645293170052
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: 0.417968888889
  y: -0.156302222222
  z: 0.484955555556
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.00713557594292
  y: -0.00407747196738
  z: 0.0825688073394
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 46
  stamp: 
    secs: 1626530999
    nsecs: 852014064
  frame_id: "imu_tf"
orientation: 
  x: -0.681735853158
  y: 0.319505968656
  z: -0.0996939204988
  w: 0.650548449177
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: 0.0704755555556
  y: 0.0471
  z: -0.316093333333
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.0224260958206
  y: -0.00713557594292
  z: 0.0805300713558
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 47
  stamp: 
    secs: 1626531001
    nsecs: 854240894
  frame_id: "imu_tf"
orientation: 
  x: -0.454047269941
  y: 0.783022220112
  z: 0.235320427203
  w: 0.354036122465
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: -0.0641955555556
  y: -0.298997777778
  z: -0.0327955555556
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.00917431192661
  y: -0.00407747196738
  z: 0.0713557594292
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 48
  stamp: 
    secs: 1626531003
    nsecs: 860127925
  frame_id: "imu_tf"
orientation: 
  x: 0.556760073269
  y: 0.5849542456
  z: 0.0198139731453
  w: -0.58945242203
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: 0.334584444444
  y: 0.742435555556
  z: 0.0987355555556
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.0193679918451
  y: -0.00305810397554
  z: 0.0937818552497
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 49
  stamp: 
    secs: 1626531005
    nsecs: 864449024
  frame_id: "imu_tf"
orientation: 
  x: 0.557316600879
  y: 0.27952214224
  z: -0.426929689901
  w: -0.654978334194
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: 0.135368888889
  y: 0.487397777778
  z: 0.22608
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.0214067278287
  y: -0.00203873598369
  z: 0.0927624872579
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 50
  stamp: 
    secs: 1626531007
    nsecs: 868388891
  frame_id: "imu_tf"
orientation: 
  x: -0.357607394719
  y: 0.279009383672
  z: -0.788473541434
  w: 0.415427718772
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: 0.236546666667
  y: 0.725688888889
  z: 0.27632
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.0428134556575
  y: -0.00101936799185
  z: 0.0927624872579
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 51
  stamp: 
    secs: 1626531009
    nsecs: 871285915
  frame_id: "imu_tf"
orientation: 
  x: 0.552268484816
  y: -0.50415504845
  z: 0.662124680716
  w: -0.0491743326098
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: -0.526822222222
  y: -0.380637777778
  z: 0.578108888889
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: 0.0316004077472
  y: 0.0163098878695
  z: 0.118246687054
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 52
  stamp: 
    secs: 1626531011
    nsecs: 875279903
  frame_id: "imu_tf"
orientation: 
  x: -0.77686512505
  y: -0.0825240625625
  z: 0.414767659018
  w: 0.466517036787
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: -0.0572177777778
  y: 0.01256
  z: -0.0711733333333
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.00407747196738
  y: 0.00203873598369
  z: 0.102956167176
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 53
  stamp: 
    secs: 1626531013
    nsecs: 879887104
  frame_id: "imu_tf"
orientation: 
  x: -0.717403886308
  y: -0.00150505802774
  z: 0.455349794699
  w: 0.527243741715
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: -0.0593111111111
  y: 0.0157
  z: -0.0697777777778
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.00407747196738
  y: 0.00305810397554
  z: 0.102956167176
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 54
  stamp: 
    secs: 1626531015
    nsecs: 882448911
  frame_id: "imu_tf"
orientation: 
  x: -0.737145736156
  y: 0.118395734138
  z: 0.541692851865
  w: 0.386222045
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: -0.0537288888889
  y: 0.0122111111111
  z: -0.0621022222222
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.00407747196738
  y: 0.00203873598369
  z: 0.102956167176
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 55
  stamp: 
    secs: 1626531017
    nsecs: 885736942
  frame_id: "imu_tf"
orientation: 
  x: -0.390389642793
  y: 0.889328068797
  z: 0.231064076962
  w: -0.0574535045685
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: -0.0380288888889
  y: 0.0233755555556
  z: -0.0694288888889
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.0
  y: 0.00509683995923
  z: 0.0998980632008
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 56
  stamp: 
    secs: 1626531019
    nsecs: 888540983
  frame_id: "imu_tf"
orientation: 
  x: -0.732930313962
  y: -0.0524445604716
  z: 0.573832360556
  w: 0.361633993053
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: -0.0655911111111
  y: 0.0174444444444
  z: -0.0648933333333
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.00407747196738
  y: 0.00305810397554
  z: 0.101936799185
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 57
  stamp: 
    secs: 1626531021
    nsecs: 890197038
  frame_id: "imu_tf"
orientation: 
  x: -0.729877806103
  y: -0.0159826419877
  z: 0.549815015863
  w: 0.405864991894
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: -0.0624511111111
  y: 0.0163977777778
  z: -0.0701266666667
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.00407747196738
  y: 0.00305810397554
  z: 0.102956167176
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 58
  stamp: 
    secs: 1626531023
    nsecs: 905812025
  frame_id: "imu_tf"
orientation: 
  x: -0.735031245674
  y: 0.0467568697874
  z: 0.398654400022
  w: 0.546459085709
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: -0.0673355555556
  y: 0.0146533333333
  z: -0.0673355555556
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.00407747196738
  y: 0.00305810397554
  z: 0.102956167176
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
header: 
  seq: 59
  stamp: 
    secs: 1626531025
    nsecs: 901469945
  frame_id: "imu_tf"
orientation: 
  x: -0.715495113192
  y: 0.0498188024865
  z: 0.514689800853
  w: 0.469765088971
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity: 
  x: -0.0638466666667
  y: -0.000697777777778
  z: -0.0771044444444
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration: 
  x: -0.00407747196738
  y: 0.00305810397554
  z: 0.101936799185
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
---
